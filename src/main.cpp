/*
 * main.cpp
 *
 *  Created on: Mar 31, 2018
 *      Author: h
 */
#include "data_crawler.h"
#include "data_table.h"
#include "http_request_sync.h"
#include "kline_data.h"
#include "utility/config.h"
#include "utility/live_data_websocket_streamer.h"

#include <csignal>

std::ostream& operator<<(std::ostream& stream, const data_table<int64_t>::interval& data)
{
    stream << " first=" << data.first << "| last=" << data.last << " ";
    return stream;
}

/*
        std::string DataBaseURI {"mongodb://localhost:27017"};
        std::string DataBaseDescriptor {"testDB"};
        std::string DataBaseCollection {"testCollection"};

        std::string webAPIHost {"api.binance.com"};
        std::string webAPITarget {"/api/v1/klines"};
*/

bool shallShuthdown{ false };

void requestShutDown(int signal)
{
    std::cout << "ShutDown requested! Will shutdown after saving data." << std::endl;
    shallShuthdown = true;
}

int main(int ac, char** av)
{
    std::signal(SIGTERM, &requestShutDown);
    std::signal(SIGABRT, &requestShutDown);
    std::signal(SIGINT, &requestShutDown);
    config conf{};
    conf.parse(ac, av);
    data_crawler<kline_data> crawler{ conf.getWebApiHost(), conf.getWebApiTarget(), conf.get_symbol() };

    std::cout << "Starting data_crawler..." << std::endl;

    auto intervals = conf.getIntervals();

    for (auto interval : intervals) {
        crawler.add_requested_category(interval);
        std::cout << "added Interval:" << to_string(interval) << std::endl;
    }

    if (!crawler.connect_db(conf.getDataBaseUri(), conf.get_symbol())) {
        std::cout << "Aborting: Crawler could not connect to db" << std::endl;
        return 1;
    }

    if (conf.isfirst_initRequested()) {
        crawler.first_init(1500007100000, 1523011859999);
    } else {
        if (!crawler.restore_data_tables()) {
            std::cout << "Aborting: Crawler failed to load dTable" << std::endl;
            return 1;
        } else {
            std::cout << "successfully restored dTable!" << std::endl;
            crawler.print_status();
        }
    }

    crawler.setup_streamer(conf.getStreamHost(), conf.getStreamPort(), conf.get_symbol());

    std::signal(SIGTERM, &requestShutDown);
    std::signal(SIGABRT, &requestShutDown);
    std::signal(SIGINT, &requestShutDown);

    try {

        while (true) {

            try {
                crawler.run();
            } catch (const rate_limit_exception& err) {
                std::cout << "Tomany requests response" << std::endl;
                // FIXME force crawler to stop sending request for a while
                // while the stream listener should continue
            }

            if (shallShuthdown) {
                std::cout << "shutting down" << std::endl;
                return 0;
            }
        }

    } catch (const char* err) {
        std::cout << "catched error:" << err << std::endl;
    }
}
