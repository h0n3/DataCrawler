//
// Created by h on 26.11.20.
//
#include "kline_data.h"

#include "data_crawler.h"

template <>
void data_crawler<kline_data>::handle_stream_data(const kline_data::stream_response_t& data)
{
    auto& actualData = data.second;
    auto& interval   = data.first;

    _mq_update_streamers[interval].push_update(actualData);

    //(*actualData).PrintDebugString();

    if ((*actualData).is_closed()) {
        auto range = get_data_range(actualData);

        (*actualData).PrintDebugString();

        auto gap = std::make_pair(interval, range);
        if (try_to_claim_data_range(gap)) {
            insert_into_db(actualData, to_string(interval));
            save_data_table(interval);
            print_status();
        }
    }
}

template <>
typename data_crawler<kline_data>::http_request_t::parameters_t
data_crawler<kline_data>::construct_request_params(
    int64_t from, kline_interval interval, std::size_t amount)
{
    return http_request_sync<kline_data>::parameters_t{ { "symbol", _symbol },
        { "interval", to_string(interval) }, { "startTime", std::to_string(from) },
        { "limit", std::to_string(amount) } };
}