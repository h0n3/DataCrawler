/*
 * config.h
 *
 *  Created on: Apr 12, 2018
 *      Author: h
 */

#ifndef UTILITY_CONFIG_H_
#define UTILITY_CONFIG_H_

#include <iostream>
#include <string>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

class config {
public:
    config()
        : _first_init{ false }
    {
        fillOptions();
    }

    void parse(int argumentCount, char** argumentData);

    bool isfirst_initRequested() const
    {
        return _first_init;
    }

    const std::string& get_symbol() const
    {
        return _symbol;
    }

    const std::vector<kline_interval>& getIntervals() const
    {
        return _intervals;
    }

    const std::string& getDataBaseDescriptor() const
    {
        return _dataBaseDescriptor;
    }

    const std::string& getDataBaseUri() const
    {
        return _dataBaseURI;
    }

    const std::string& getWebApiHost() const
    {
        return _webAPIHost;
    }

    const std::string& getWebApiTarget() const
    {
        return _webAPITarget;
    }

    const std::string& getStreamHost() const
    {
        return _streamHost;
    }

    const std::string& getStreamPort() const
    {
        return _streamPort;
    }

private:
    po::options_description _desc;
    po::variables_map       _vm;

    bool _first_init;

    std::string _dataBaseURI;
    std::string _dataBaseDescriptor;

    std::string _webAPIHost;
    std::string _webAPITarget;

    std::string _streamHost;
    std::string _streamPort;

    std::string                 _symbol;
    std::vector<kline_interval> _intervals;

    void fillOptions();

    void loadIntervals();

    template <class T>
    void checkAndSetArgument(const char* name, T& data)
    {
        if (_vm.count(name)) {
            data = _vm[name].as<T>();
        } else {
            std::cout << "missing option \"" << name << "\"" << std::endl;
            printHelpAndExit();
        }
    }

    void printHelpAndExit()
    {
        std::cout << _desc << std::endl;
        std::exit(1);
    }
};

void config::parse(int argumentCount, char** argumentData)
{
    po::store(po::parse_command_line(argumentCount, argumentData, _desc), _vm);
    po::notify(_vm);

    if (_vm.count("help")) {
        printHelpAndExit();
    }

    _first_init = _vm.count("create-dTable");

    std::string path{};
    if (_vm.count("cfg-path")) {
        path = _vm["cfg-path"].as<std::string>();

    } else {
        path = "test.cfg";
    }

    po::store(po::parse_config_file<std::string::value_type>(path.c_str(), _desc, false), _vm);

    checkAndSetArgument("symbol", _symbol);

    loadIntervals();

    checkAndSetArgument("db-URI", _dataBaseURI);
    checkAndSetArgument("api-host", _webAPIHost);
    checkAndSetArgument("api-target", _webAPITarget);
    checkAndSetArgument("stream-host", _streamHost);
    checkAndSetArgument("stream-port", _streamPort);
}

void config::fillOptions()
{
    po::options_description commands{ "Commands:" };
    commands.add_options()("help,h", "produce help message")(
        "create-dTable", "forces the tool to create a new dTable");

    po::options_description optional{ "Optional:" };
    optional.add_options()(
        "cfg-path,c", po::value<std::string>(), "Set configfile path to load options from");

    po::options_description required{ "Required: (Either in the config File or as CMD parameter)" };
    required.add_options()("symbol,s", po::value<std::string>(), "Specify the trade symbol ")(
        "intervals,i", po::value<std::vector<std::string>>()->composing()->multitoken(),
        "Specify the intervals of the data")("db-URI", po::value<std::string>(),
        "Specify database to connect to")("api-host", po::value<std::string>(), "Specify api host name")(
        "api-target", po::value<std::string>(), "Specify api target")(
        "stream-host,sh", po::value<std::string>(), "Specify the stream host")(
        "stream-port,sp", po::value<std::string>(), "Specify the stream port");

    _desc.add(commands).add(optional).add(required);
}

void config::loadIntervals()
{
    try {
        if (_vm.count("intervals")) {
            auto data = _vm["intervals"].as<std::vector<std::string>>();
            for (auto& entry : data) {
                _intervals.push_back(parse_interval(entry));
            }
        } else {
            std::cout << "missing option \"intervals\"" << std::endl;
            printHelpAndExit();
        }
    } catch (const std::exception& error) {
        std::cout << "Error loading config:" << error.what() << std::endl;
        std::abort();
    }
}

#endif /* UTILITY_CONFIG_H_ */
