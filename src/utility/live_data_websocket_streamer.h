/*
 * live_data_websocket_streamer.h
 *
 *  Created on: Apr 13, 2018
 *      Author: h
 */

#ifndef LIVEDATAWEBSOCKETSTREAMER_H_
#define LIVEDATAWEBSOCKETSTREAMER_H_

#include <functional>
#include <memory>

#include "rapidjson/document.h"

#include "ws_session_async.h"

#include <algorithm>
#include <string>

template <class requested_t>
class live_data_websocket_streamer {
public:
    using stream_response_t = typename requested_t::stream_response_t;

    using on_data_handler_t = std::function<void(const stream_response_t&)>;

    live_data_websocket_streamer(boost::asio::io_context& ioc, boost::asio::ssl::context& ctx);

    ~live_data_websocket_streamer() { }

    void register_on_data_handler(on_data_handler_t on_data_handler)
    {
        _on_data_handler = on_data_handler;
    }

    void set_symbol(const std::string& symbol)
    {
        _symbol = symbol;
        std::transform(_symbol.begin(), _symbol.end(), _symbol.begin(), ::tolower);
    }

    void add_target(const std::string target)
    {
        _targets.push_back(target);
    }

    void connect(const std::string& host, const std::string& port);

    void handlePacket(const std::string& data);

private:
    boost::asio::io_context&   _ioc;
    boost::asio::ssl::context& _ctx;

    std::shared_ptr<ws_session_async> _session;

    on_data_handler_t _on_data_handler;

    std::string              _symbol;
    std::vector<std::string> _targets;

    std::string build_address();
};

template <class requested_t>
live_data_websocket_streamer<requested_t>::live_data_websocket_streamer(
    boost::asio::io_context& ioc, boost::asio::ssl::context& ctx)
    : _ioc{ ioc }
    , _ctx{ ctx }
{
    ws_session_async::read_handler_t read_handler
        = std::bind(&live_data_websocket_streamer::handlePacket, this, std::placeholders::_1);

    _session = std::make_shared<ws_session_async>(_ioc, _ctx, read_handler);
}

template <class requested_t>
void live_data_websocket_streamer<requested_t>::connect(const std::string& host, const std::string& port)
{
    std::string address = build_address();

    std::cout << "Requesting Stream with address:" << host << ":" << port << address << std::endl;

    _session->run(host, port, address);
}

template <class requested_t>
void live_data_websocket_streamer<requested_t>::handlePacket(const std::string& data)
{
    if (data.size() && _on_data_handler) {
        try {
            rapidjson::Document document{};
            document.Parse(data.c_str());

            if (!document.HasMember("stream")) {
                throw "invalid packet"; // FIXME
            }

            stream_response_t response = requested_t::parse_stream_version(document["data"]);

            // std::cout << "recieved stream data:" << to_string(response.second) <<
            // std::endl; std::cout << response.first << std::endl;

            _on_data_handler(response);

        } catch (const char* errorMsg) {
            std::cout << "failed to parse packet errormsg:" << errorMsg
                      << std::endl; // FIXME add better exceptions
        } catch (...) {
            std::cout << "failed to parse packet" << std::endl;
        }
    }
}

template <class requested_t>
std::string live_data_websocket_streamer<requested_t>::build_address()
{
    if (_targets.empty())
        throw "Tried to start live_data_websocket_streamer without targets"; // FIXME

    std::stringstream addressBuilder{};
    addressBuilder << "/stream?streams=";

    bool isFirst{ true };
    for (auto& target : _targets) {
        addressBuilder << (isFirst ? "" : "/") << _symbol << "@" << target;
        isFirst = false;
    }

    return addressBuilder.str();
}

#endif /* LIVEDATAWEBSOCKETSTREAMER_H_ */
