/*
 * data_crawler.h
 *
 *  Created on: Apr 5, 2018
 *      Author: h
 */

#ifndef DATACRAWLER_H_
#define DATACRAWLER_H_

#include <limits>

#include "data_table.h"
#include "http_request_sync.h"
#include "kline_interval.h"
#include "live_data_mq_streamer.h"
#include "mongodb_connector.h"
#include "utility/live_data_websocket_streamer.h"

template <class requested_t>
class data_crawler {
public:
    using order_t              = typename requested_t::order_t;
    using data_table_t         = data_table<order_t>;
    using interval_t           = typename data_table_t::interval;
    using http_request_t       = http_request_sync<requested_t>;
    using ws_streamer_t        = live_data_websocket_streamer<requested_t>;
    using category_t           = typename requested_t::category_t;
    using gap_t                = std::pair<category_t, interval_t>;
    using mq_update_streamer_t = live_data_mq_streamer<requested_t>;

    data_crawler(const std::string& host, const std::string& target, const std::string& symbol);

    void add_requested_category(category_t category);

    void first_init(order_t first, order_t last);

    bool restore_data_tables();

    typename http_request_t::parameters_t construct_request_params(
        int64_t from, kline_interval interval, std::size_t amount);

    bool connect_db(std::string uri, std::string dataBase);

    void setup_streamer(const std::string& host, const std::string& port, const std::string& symbol);

    void handle_stream_data(const typename ws_streamer_t::stream_response_t& data);

    void run();

    void print_status();

    void print_status(const data_table_t& data_table);

private:
    boost::asio::io_context   _io_context;
    boost::asio::ssl::context _ssl_context;

    ws_streamer_t     _ws_streamer;
    mongodb_connector _db_connector;

    std::map<category_t, data_table_t>         _data_tables;
    std::map<category_t, mq_update_streamer_t> _mq_update_streamers;

    std::string _host;
    std::string _target;
    std::string _symbol;

    void save_data_table(category_t interval);

    boost::optional<gap_t> find_first_gap();

    boost::optional<std::vector<requested_t>> request_data(const gap_t& gap);

    interval_t get_data_range(const requested_t& data);

    interval_t get_data_range(const std::vector<requested_t>& data);

    void trunc_data_range(std::vector<requested_t>& data, order_t last_unclaimed_id);

    bool try_to_claim_data_range(const gap_t& gap);

    void insert_into_db(const requested_t& data, const std::string& collection);

    void insert_into_db(const std::vector<requested_t>& data, const std::string& collection);
};

#include "data_crawler_impl.h"

#endif /* DATACRAWLER_H_ */
