/*
 * data_crawler.cpp
 *
 *  Created on: Apr 5, 2018
 *      Author: h
 */

#include "data_crawler.h"

template <class requested_t>
data_crawler<requested_t>::data_crawler(
    const std::string& host, const std::string& target, const std::string& symbol)
    : _data_tables{}
    , _db_connector{}
    , _io_context{}
    , _ssl_context{ boost::asio::ssl::context::sslv23_client }
    , _ws_streamer{ _io_context, _ssl_context }
    , _host{ host }
    , _target{ target }
    , _symbol{ symbol }
{
    load_root_certificates(_ssl_context);
}

template <class requested_t>
bool data_crawler<requested_t>::connect_db(std::string uri, std::string dataBase)
{
    return _db_connector.connect(uri, dataBase);
}

template <class requested_t>
void data_crawler<requested_t>::add_requested_category(category_t category)
{
    _data_tables.emplace(std::piecewise_construct, std::forward_as_tuple(category),
        std::forward_as_tuple(order_t{ 0 }, order_t{ 0 }, requested_t::get_gap_distance(category)));

    auto result = _mq_update_streamers.emplace(
        std::piecewise_construct, std::forward_as_tuple(category), std::forward_as_tuple());

    result.first->second.open(_symbol, category, 1u);

    _ws_streamer.add_target(requested_t::get_stream_target(category));
}

template <class requested_t>
void data_crawler<requested_t>::first_init(order_t first, order_t last)
{
    for (auto& table_iter : _data_tables) {
        auto& d_table = table_iter.second;

        d_table.set_start_point(first);
        d_table.set_end_point(last);
        save_data_table(table_iter.first);
    }

    print_status();
}

template <class requested_t>
bool data_crawler<requested_t>::restore_data_tables()
{
    for (auto& data_table_iter : _data_tables) {
        auto& d_table  = data_table_iter.second;
        auto  category = data_table_iter.first;

        _db_connector.set_collection(requested_t::get_db_collection_name(category));
        auto d_table_state = _db_connector.find_one(d_table.get_db_filter());
        if (d_table_state) {
            d_table.load(d_table_state.value());
        } else {
            return false;
        }
    }
    return true;
}

template <class requested_t>
void data_crawler<requested_t>::setup_streamer(
    const std::string& host, const std::string& port, const std::string& symbol)
{
    _ws_streamer.set_symbol(symbol);

    _ws_streamer.register_on_data_handler(
        std::bind(&data_crawler::handle_stream_data, this, std::placeholders::_1));

    _ws_streamer.connect(host, port);
}

template <class requested_t>
void data_crawler<requested_t>::run()
{
    auto gap = find_first_gap();

    if (gap) {
        auto& active_gap = gap.get();
        auto  category   = active_gap.first;

        auto data = request_data(active_gap);
        if (data) {
            auto& d_table             = _data_tables[category];
            auto& active_gap_interval = active_gap.second;

            auto&      active_data = data.get();
            interval_t range       = get_data_range(active_data);

            std::cout << "Got DataPacket Range=" << range << std::endl;
            std::cout << active_gap_interval << std::endl;

            if (range.last > d_table.last()) {
                d_table.set_end_point(range.last);
            }

            if ((range.last > active_gap_interval.last)) {
                trunc_data_range(active_data, active_gap_interval.last);
                range = get_data_range(active_data);
                std::cout << "recieved to much data runcating new data range:" << range << std::endl;
            }

            gap_t gap_to_claim{ category, range };

            auto& ref = gap_to_claim.first;

            // std::cout << "trying to claim gap on dTableId:" << gap_to_claim.second
            // << " = " << ref << std::endl;

            if (try_to_claim_data_range(gap_to_claim)) {
                insert_into_db(active_data, requested_t::get_db_collection_name(category));
                save_data_table(category);

                print_status();
            } else {
                std::cout << "tried to claim " << ref << " already claimed interval" << std::endl;
                d_table.print_entrys();
            }
        }
    }

    _io_context.run_for(std::chrono::milliseconds(200));
}

template <class requested_t>
void data_crawler<requested_t>::print_status()
{
    std::cout << "______________________________________________DataCrawler::"
                 "Status______________________________________________"
              << std::endl;
    for (auto table_iter : _data_tables) {
        print_status(table_iter.second);
    }
    std::cout << "_______________________________________________________________"
                 "________________________________________________"
              << std::endl;
}

template <class requested_t>
void data_crawler<requested_t>::print_status(const data_table_t& d_table)
{
    int64_t first       = d_table.first();
    int64_t last        = d_table.last();
    int64_t range       = last - first;
    int64_t claimed_ids = d_table.num_claimed_ids();

    std::cout << "Got " << claimed_ids << " Elements of the requested Range [" << first << ", " << last
              << "]. Completed:"
              << boost::lexical_cast<std::string>(
                     (static_cast<double>(claimed_ids) / static_cast<double>(range)) * 100.0)
              << "%" << std::endl;

    d_table.print_entrys();
}

template <class requested_t>
void data_crawler<requested_t>::save_data_table(category_t table_id)
{
    _db_connector.set_collection(requested_t::get_db_collection_name(table_id));

    auto&                    d_table          = _data_tables[table_id];
    bsoncxx::document::value data_table_entry = d_table.encode();
    _db_connector.replace(d_table.get_db_filter(), data_table_entry.view());
}

template <class requested_t>
boost::optional<typename data_crawler<requested_t>::gap_t> data_crawler<requested_t>::find_first_gap()
{
    for (auto data_table_iter : _data_tables) {
        auto  category = data_table_iter.first;
        auto& d_table  = data_table_iter.second;

        auto gap = d_table.first_gap();
        if (gap) {
            return std::make_pair(category, gap.get());
        }
    }
    return {};
}

template <class requested_t>
boost::optional<std::vector<requested_t>> data_crawler<requested_t>::request_data(const gap_t& gap)
{
    category_t category     = gap.first;
    auto&      requestedGap = gap.second;

    auto request_params = construct_request_params(requestedGap.first, category, 3000);
    try {
        http_request_t http_request{ _io_context, _ssl_context };
        return http_request.request_data(_host, _target, request_params);

    } catch (const invalid_response_exception& err) {
        std::cout << err.what() << std::endl;
        std::abort();
    }
}

template <class requested_t>
typename data_crawler<requested_t>::interval_t data_crawler<requested_t>::get_data_range(
    const requested_t& data)
{
    return interval_t{ requested_t::get_lower_order_value(data),
        requested_t::get_higher_order_value(data) };
}

template <class requested_t>
typename data_crawler<requested_t>::interval_t data_crawler<requested_t>::get_data_range(
    const std::vector<requested_t>& data)
{
    order_t highest = std::numeric_limits<order_t>::min();
    order_t lowest  = std::numeric_limits<order_t>::max();
    for (auto entry : data) {
        order_t lower_value = requested_t::get_lower_order_value(entry);
        if (lower_value < lowest)
            lowest = lower_value;

        order_t higher_value = requested_t::get_higher_order_value(entry);
        if (higher_value > highest)
            highest = higher_value;
    }
    return interval_t{ lowest, highest };
}

template <class requested_t>
void data_crawler<requested_t>::trunc_data_range(
    std::vector<requested_t>& data, order_t last_unclaimed_id)
{
    for (auto iter = data.begin(); iter < data.end();) {
        if (requested_t::get_higher_order_value(*iter) > last_unclaimed_id) {
            data.erase(iter);
        } else {
            ++iter;
        }
    }
}

template <class requested_t>
bool data_crawler<requested_t>::try_to_claim_data_range(const gap_t& gap)
{
    const auto category = gap.first;
    auto&      table    = _data_tables[category];

    const auto& interval = gap.second;
    return table.claim(interval);
}

template <class requested_t>
void data_crawler<requested_t>::insert_into_db(const requested_t& data, const std::string& collection)
{
    _db_connector.set_collection(collection);

    bsoncxx::document::value document = requested_t::convert_to_db_entry(data);
    _db_connector.insert(document.view());
}

template <class requested_t>
void data_crawler<requested_t>::insert_into_db(
    const std::vector<requested_t>& data, const std::string& collection)
{
    _db_connector.set_collection(collection);

    for (auto& entry : data) {
        bsoncxx::document::value document = requested_t::convert_to_db_entry(entry);
        _db_connector.insert(document.view());
    }
}
